# Forecasting system GP

This project was created for Academy of Law Enforcement to forecast if person will make repeated crime.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


1. Create and activate virtual environment:
 ```shell
$ virtualenv -p python 3 ../environments/fs_GP_env
$ source ../environments/fs_GP_env/bin/activate
```

2. Install all needed packages:
```shell
$ pip install -r requirements.txt
```

3 Create database and user
```shell
 $ sudo -u postgres psql
 $ postgres=# create database forecasting_system_gp;
 $ postgres=# create user prokuror with encrypted password 'miras3262';
 $ postgres=# grant all privileges on database forecasting_system_gp to prokuror;
```
4 Run server on localhost
```shell
 $ python manage.py runserver
```



## Authors

* **Yessenkeldi Miras** 


