from django.apps import AppConfig


class CheckingPeopleConfig(AppConfig):
    name = 'checking_people'
