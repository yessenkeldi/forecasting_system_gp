from django.urls import path
from checking_people import views

urlpatterns = [
    path('list',views.list_view, name='list'),
    path('', views.index, name='index')
]