from django.shortcuts import render
from checking_people.models import CriminalListFinal

# Create your views here.
def index(request):
    return render(request,'checking_people/index.html')

def list_view(request):
    criminals_list = CriminalListFinal.objects.order_by('id')
    dict = {
        'criminals_list': criminals_list
    }
    return render(request,'checking_people/list.html',context=dict)
    # return render(request, 'checking_people/list.html')