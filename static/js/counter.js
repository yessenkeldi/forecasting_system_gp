
question_list = {question1:"",question2:"",question3:"",question4:"",question5:"",question6:"",question7:"",question8:"",
                question9:"",question10:"",question11:"",question12:"",question13:""};



$('#result').click(function () {
    question_list["question1"] = $("#question_1")[0].value;
    question_list["question2"] = $("#question_2")[0].value;
    for (var k in question_list) {
            if (k === "question1" || k === "question2"){

            }else{
            var some_question = $('input[name="' + k + '"]');
            check_if_result(some_question, k)
                }
    }

    if (is_they_answer() === true){
        results = count_result();
        $('#result_div').css('display','block');
        $('#result_1')[0].outerText = results[0];
        $('#result_2')[0].outerText = results[1];
        $('#result_3')[0].outerText = results[2];
        $('#resultant_name')[0].outerText =question_list["question1"]
    }
})

function check_if_result(question,question_name) {
    var answer = '';
    for (var k = 0;k<question.length;k++ ){
        if (question[k].checked === true){
            answer = question[k].value;
        }

    }
    question_list[question_name] = answer;
}

function turn_all_black() {
    for (var k in question_list){
        var turn_black = $('#'+k);
        turn_black.css('color','black')
        $('#warning_message')[0].innerText = ""
    }
}


function is_they_answer() {
    var is_all_answered = true;
    turn_all_black()
    for (var k in question_list){
        if (question_list[k]===''){
            var turn_red = $('#'+k);
            is_all_answered = false;
            turn_red.css('color','red');
            $('#warning_message')[0].innerText = "Пожалуйста ответьте на все вопросы!"
        }
    }

    return is_all_answered
}

function count_result() {
    var category_1;
    var category_2;
    var category_3;
    category_1 = (1.177 * question_list["question2"]) + (17.528 * question_list["question3"]) +
                 (5.579 * question_list["question4"]) + (0.786 * question_list["question5"]) +
                 (40.041 * question_list["question6"]) + (41.556 * question_list["question7"]) +
                 (10.161 * question_list["question8"]) + (1.873 * question_list["question9"]) +
                 (6.034 * question_list["question10"]) + (12.631 * question_list["question11"])+
                 (5.984 * question_list["question12"]) + (10.125 * question_list["question13"])-172.239;
    category_2 = (1.190 * question_list["question2"]) + (18.475 * question_list["question3"]) +
                 (4.667 * question_list["question4"]) + (0.974 * question_list["question5"]) +
                 (41.393 * question_list["question6"]) + (41.794 * question_list["question7"]) +
                 (10.595 * question_list["question8"]) + (1.765 * question_list["question9"]) +
                 (5.951 * question_list["question10"]) + (12.629 * question_list["question11"])+
                 (6.045 * question_list["question12"]) + (9.769 * question_list["question13"]) - 174.869;
    category_3 = (1.110 * question_list["question2"]) + (18.124 * question_list["question3"]) +
                 (4.855 * question_list["question4"]) + (1.390 * question_list["question5"]) +
                 (41.786 * question_list["question6"]) + (42.070 * question_list["question7"]) +
                 (12.307 * question_list["question8"]) + (1.873 * question_list["question9"]) +
                 (4.522 * question_list["question10"]) + (10.719 * question_list["question11"])+
                 (5.668 * question_list["question12"]) + (9.537 * question_list["question13"]) - 164.554;
    return [category_1,category_2,category_3]

}

$("#restart_button").click(function () {
    for (k in question_list){
        question_list[k] = "";
    }
    $("#question_1")[0].value = "";
    $("#question_2")[0].value = "";
    $(".form-check-input").prop('checked', false);
    $('#result_div').css('display','None');
    turn_all_black()
});


